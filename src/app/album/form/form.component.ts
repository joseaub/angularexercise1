import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-albumform',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class MapFormComponent {

  url = new FormControl('');

  sendUrl() {
    this.url.setValue('')
  }
}
