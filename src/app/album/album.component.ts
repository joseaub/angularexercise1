import {Component, OnInit } from '@angular/core';
// import {MatDialog, MatDialogConfig, MatCard, MatCardHeader} from "@angular/material";
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


class Image {
  id: number;
  url: string;
  thumbnailUrl: string;
}

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit{
  albumObservable : Observable<Image[]>;
  // theImages = [];
  constructor( private httpClient: HttpClient) {}

  ngOnInit() {
    this.albumObservable = this.httpClient.get<Image[]>("https://jsonplaceholder.typicode.com/photos");
  }
  // openDialog() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   this.dialog.open(DialogImage, dialogConfig);
  // }
}
